import { RECEIVE_DATA } from '../actions/index';

export const data = (state = {}, { type, data }) => {
  switch (type) {
    case RECEIVE_DATA:
      return data;

    default:
      return state;
  }
};
