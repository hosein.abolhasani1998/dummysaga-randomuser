import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { requestData } from '../store/actions/index';
const Home = (props) => {
  useEffect(() => {
    props.requestData();
    //eslint-disable-next-line
  }, []);
  const { results = [] } = props.data;
  return (
    <div className="App">
      <header className="App-header">
        {results.length ? (
          <h1>
            {results.map((result) => (
              <div key={result.id.value}>
                <p>Gender :{result.gender}</p>
                <p>FirstName :{result.name.first}</p>
                <p>LastName :{result.name.last}</p>
                <img src={result.picture.medium} alt="person" />
              </div>
            ))}
          </h1>
        ) : (
          <h1>Loading ...</h1>
        )}
      </header>
    </div>
  );
};
const mapStatToProps = (state) => ({ data: state.data });

export default connect(mapStatToProps, { requestData })(Home);
