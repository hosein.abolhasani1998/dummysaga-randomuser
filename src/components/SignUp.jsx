import React, { useState } from 'react';
import { useForm } from 'react-hook-form';

const SignUp = () => {
  const { register, handleSubmit, errors } = useForm();
  const [data, setData] = useState();
  const onSubmit = (data) => {
    setData(data);
  };

  return (
    <div className="segment" style={{ marginTop: '3.5rem' }}>
      <form onSubmit={handleSubmit(onSubmit)} className="ui form  container ">
        <div className=" field">
          <label>First Name</label>
          <input
            type="text"
            name="firstName"
            placeholder="First Name"
            ref={register}
            className="ui  inverted red"
          />
        </div>
        <div className=" field">
          <label>Last Name</label>
          <input
            style={{ marginBottom: '1.25rem' }}
            type="text"
            name="lastName"
            placeholder="Last Name"
            ref={register({ required: true })}
          />
          {errors.lastName && (
            <span className="ui red  message small">
              Please Fill this feild
            </span>
          )}
        </div>
        <div className=" field">
          <label>Phone </label>
          <input
            style={{ marginBottom: '1.25rem' }}
            type="tel"
            name="phone"
            placeholder="phone number"
            ref={register({
              required: true,
              pattern: {
                value: /(0|\+98)?([ ]|-|[()]){0,2}9[1|2|3|4]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}/gi,
                message: 'please enter valid number',
              },
            })}
          />
          {errors.phone && errors.phone.type === 'required' && (
            <span className="ui red  message small">
              this feild is required
            </span>
          )}
          {errors.phone && errors.phone.type === 'pattern' && (
            <span className="ui red  message small">
              Please enter a valid number
            </span>
          )}
        </div>
        <div className="inline field ">
          <label>isDeveloper :</label>
          <input type="checkbox" name="isDeveloper" ref={register} />
        </div>
        <button className="ui button inverted red" type="submit">
          Submit
        </button>
      </form>
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </div>
  );
};

export default SignUp;
