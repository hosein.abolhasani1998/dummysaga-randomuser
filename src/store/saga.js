import { takeLatest, call, put } from 'redux-saga/effects';
import { receiveData, REQUEST_DATA } from './actions';
import fetchData from './api/index';

function* getApiData() {
  try {
    const data = yield call(fetchData);
    yield put(receiveData(data));
  } catch (error) {
    console.log(error);
  }
}

export default function* mySaga() {
  yield takeLatest(REQUEST_DATA, getApiData);
}
