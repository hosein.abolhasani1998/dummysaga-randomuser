export const RECEIVE_DATA = 'RECEIVET_DATA';
export const REQUEST_DATA = 'REQUEST_DATA';

export const requestData = () => ({ type: REQUEST_DATA });
export const receiveData = (data) => ({ type: RECEIVE_DATA, data });
