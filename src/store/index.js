import { createStore, applyMiddleware } from 'redux';
import createSageMiddleware from 'redux-saga';
import reducer from './reducers/index';
import mySaga from './saga';
const saga = createSageMiddleware();

export default createStore(reducer, applyMiddleware(saga));

saga.run(mySaga);
