import React from 'react';
import { Link } from 'react-router-dom';
const Header = () => {
  return (
    <div className="ui top   fixed two item menu inverted red ">
      <Link to="/" className="item">
        Sagas
      </Link>
      <Link to="/signup" className="item">
        HookForm
      </Link>
    </div>
  );
};

export default Header;
